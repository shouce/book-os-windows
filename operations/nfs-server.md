# NFS 服务器

工具：haneWIN NFS Server

## Export file

```
E:\NFS
D:\Projects\markjour -name:markjour
```

记得设置防火墙开放对应端口，或者直接关闭防火墙。

重启服务器即可使用了。

## mount

```sh
mount catroll:/e/NFS /mnt/nfs
mount catroll:/markjour /opt/markjour
```

## 参考

- [官方文档](http://www.hanewin.net/doc/nfs/nfsd.htm)
