# 玩转 Windows

最新的 Windows 10 上手使用了好一段时间，感觉很好，尤其是 Linux 子系统，屌炸天（Win7，XP 也很好啦...）。

所以呢，这个手册以 Windows 10 为背景。

## 环境变量

大小写好像不敏感，我写成驼峰式是为了方便阅读。

### 系统环境变量

- <https://docs.microsoft.com/en-us/windows/deployment/usmt/usmt-recognized-environment-variables>
- <https://docs.microsoft.com/zh-cn/windows/deployment/usmt/usmt-recognized-environment-variables>

变量 | 值
---- | --
`HomeDrive`             | `C:`
`HomePath`              | `\Users\用户名`
`WinDir`                | `C:\Windows`
`System`                | `%WinDir%\system32`.
`System16`              | `%WinDir%\system`.
`System32`              | `%WinDir%\system32`.
`ComSpec`               | `C:\Windows\System32\cmd.exe`
`AllUsersProfile`       | `C:\ProgramData`
`ProgramData`           | `C:\ProgramData`
`ProgramFiles`          | `C:\Program Files`
`ProgramFiles(X86)`     | `C:\Program Files (x86)`
`CommonProgramFiles`    | `C:\Program Files\Common Files`
`CommonProgramFiles(x86)` | `C:\Program Files (x86)\Common Files`
`Public`                | `C:\Users\Public`
`UserProfile`           | `C:\Users\用户名`
`AppData`               | `C:\Users\用户名\AppData\Roaming`<br>曾经：<br>`C:\Documents and Settings\用户名\Application Data`
`LocalAppData`          | `C:\Users\用户名\AppData\Local`
`Temp`                  | `C:\Users\用户名\AppData\Local\Temp`
`Tmp`                   | `C:\Users\用户名\AppData\Local\Temp`
`SystemDrive`           | `C:`
`SystemRoot`            | `C:\Windows`

### 用户自定义环境变量

1. 我的电脑（右键），属性，高级系统设置，环境变量
2. 控制面板，系统和安全，系统，高级系统设置，环境变量
3. cmd，执行 `SystemPropertiesAdvanced` 命令，环境变量

- `SystemPropertiesAdvanced` -> 系统属性：高级
- `SystemPropertiesComputerName` -> 系统属性：计算机名
- `SystemPropertiesDataExecutionPrevention` -> 系统属性：性能选项：数据执行保护
- `SystemPropertiesHardware` -> 系统属性：硬件
- `SystemPropertiesPerformance` -> 系统属性：：性能选项：视觉效果
- `SystemPropertiesProtection` -> 系统属性：系统保护
- `SystemPropertiesRemote` -> 系统属性：远程
