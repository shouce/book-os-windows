# netsh

## `wlan show`

```sh
C:\WINDOWS\system32>netsh wlan show

下列指令有效:

此上下文中的命令:
show all       - 显示完整的无线设备和网络信息。
show allowexplicitcreds - 显示允许共享用户凭据设置。
show autoconfig - 显示是否启用或禁用自动配置逻辑。
show blockednetworks - 显示阻止的网络显示设置。
show createalluserprofile - 显示是否允许所有人创建所有
                 用户配置文件。
show drivers   - 显示系统上无线 LAN 驱动程序的属性。
show filters   - 显示允许和阻止的网络列表。
show hostednetwork - 显示承载网络的属性和状态。
show interfaces - 显示系统上的无线局域网接口
                 的列表。
show networks  - 显示计算机上可见的网络列表。
show onlyUseGPProfilesforAllowedNetworks - 显示在配置 GP 的网络设置上仅使用 GP 配置文件。
show profiles  - 显示计算机上配置的配置文件列表。
show randomization - 显示 MAC 随机化是已启用还是
                 已禁用。
show settings  - 显示无线 LAN 的全局设置。
show tracing   - 显示是否启用或禁用无线局域网跟踪。
show wirelesscapabilities - 显示系统的无线功能
show wlanreport - 生成一个报告，显示最新的无线会话信息。
```

#### `netsh wlan show drivers`

```sh
C:\WINDOWS\system32>netsh wlan show drivers

接口名称: 磊科WIFI

    驱动程序                  : Realtek RTL8191SU 无线 LAN 802.11n USB 2.0 网络适配器
    供应商                    : Realtek Semiconductor Corp.
    提供程序                  : Microsoft
    日期                      : 2013/3/30
    版本                      : 1086.51.328.2013
    INF 文件                  : net8192su64.inf
    类型                      : 本机 WLAN 驱动程序
    支持的无线电类型          : 802.11n 802.11g 802.11b
    支持 FIPS 140-2 模式: 否
    支持 802.11w 管理帧保护 : 否
    支持的承载网络  : 是
    基础结构模式中支持的身份验证和密码:
                                开放式             无
                                WPA2 - 个人       CCMP
                                开放式             WEP-40bit
                                开放式             WEP-104 位
                                开放式             WEP
                                WPA - 企业        TKIP
                                WPA - 个人        TKIP
                                WPA2 - 企业       TKIP
                                WPA2 - 个人       TKIP
                                WPA - 企业        CCMP
                                WPA - 个人        CCMP
                                WPA2 - 企业       CCMP
                                供应商定义的          TKIP
                                供应商定义的          CCMP
    临时模式中支持的身份验证和密码:
                                开放式             无
                                开放式             WEP-40bit
                                开放式             WEP-104 位
                                开放式             WEP
                                WPA2 - 个人       CCMP
    支持的无线显示器: 否 (图形驱动程序: 否，WLAN 驱动程序: 否)
```

#### `netsh wlan show drivers`

## 无线网卡分享网络

```sh
C:\WINDOWS\system32>netsh wlan set hostednetwork mode=allow ssid=303i5 key=123456789
承载网络模式已设置为允许。
已成功更改承载网络的 SSID。
已成功更改托管网络的用户密钥密码。

C:\WINDOWS\system32>netsh wlan show hostednetwork

承载网络设置
-----------------------
    模式                   : 已启用
    SSID 名称              :“303i5”
    最多客户端数  : 100
    身份验证         : WPA2 - 个人
    密码                 : CCMP

承载网络状态
---------------------
    状态                 : 已启动
    BSSID                  :08:10:7a:00:03:58
    无线电类型             :802.11n
    频道                : 11
    客户端数      : 1
        74:23:44:b0:18:2f        已经过身份验证
```
