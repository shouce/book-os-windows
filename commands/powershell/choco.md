# chocolatey

## 安装

管理员模式运行 PowerShell，然后执行下面的命令：

```powershell
# 修改执行策略。可选：
# - Unrestricted
# - RemoteSigned
# - AllSigned
# - Restricted
# - Default
# - Bypass
# - Undefined
Set-ExecutionPolicy RemoteSigned

# 下载安装脚本（Invoke-WebRequest）并执行（Invoke-Expression）
iwr https://chocolatey.org/install.ps1 -UseBasicParsing | iex
```

如果第二步报错如下：

```txt
iwr : 请求被中止: 未能创建 SSL/TLS 安全通道。
所在位置 行:1 字符: 1
+ iwr https://chocolatey.org/install.ps1 -UseBasicParsing | iex
+ ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    + CategoryInfo          : InvalidOperation: (System.Net.HttpWebRequest:HttpWebRequest) [Invoke-WebRequest]，WebException
    + FullyQualifiedErrorId : WebCmdletWebResponseException,Microsoft.PowerShell.Commands.InvokeWebRequestCommand
```

那么执行：

```powershell
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
# 相当于 C# 的
# System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
```

原因是当前系统采用的 TLS 是过时的 1.0 版。

## 使用

### 安装软件

```powershell
choco install microsoft-windows-terminal
```

# 参考

- Microsoft Docs, [Set-ExecutionPolicy](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.security/set-executionpolicy?view=powershell-7)
- StackOverflow, [Powershell Invoke-WebRequest Fails with SSL/TLS Secure Channel](https://stackoverflow.com/questions/41618766/powershell-invoke-webrequest-fails-with-ssl-tls-secure-channel)
