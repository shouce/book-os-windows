# PowerShell

## 简介

### 特点

PowerShell 语法与其背后的基础（.NET）感觉比 bash 之类的 Linux Shell 更加牢靠，命令命名都比较规范，如果更加开放，假以时日，生态建设更齐全一些，我觉得应该会成为一门不错的脚本语言。

1. 基于 .NET 平台，命令输出的是对象，不比 bash 之流字符串走天下，PS 是强类型的。
1. 命令风格：动词 + 名词，另外，继承 Windows 大小写不敏感的设计  
   PS：看到的所有单个单词都是别名  
1. 得到了想象不到的支持，彻底打通了 Windows 及其他 MS 家产品  
   得益于良好的拓展接口支持，PS 得到的第三方软件开发商支持也不少  
   可能用惯了 Unix Shell，第一次听说需要做特殊设计，可能感觉奇怪，  
   其实应该把 PS 理解成一个面向对象的语言，第三方支持就好比提供拓展库，操作其产品  
   了解一下 PS 直接操作 Word，Excel 的示例可能有更深的认识。  
   虽然弄的比 bash 复杂，但是却能实现更多可能性，可能很多人不认同这理念，见仁见智吧

### 示例

#### Word 文档转 PDF

```powershell
$DocFile = Get-Item '/tmp/book.docx'
# $PdfPath = "$(($DocFile.FullName).substring(0, $DocFile.FullName.lastIndexOf("."))).pdf"
$PdfPath = "$($DocFile.DirectoryName)/$($DocFile.BaseName).pdf"
$Word = New-Object -ComObject Word.Application
$Doc = $Word.docs.Open($DocFile.FullName)
$Doc.SaveAs($PdfPath, 17)
$Doc.Close()
$Word.Quit()
```

#### 其他示例

1. 停止所有当前运行中的以"p"字符开头命名的程序：

    ```powershell
    get-process p* | stop-process
    ```

1. 停止所有当前运行中的所有使用大于1000MB存储器的程序：

    ```powershell
    get-process | where { $_.WS -gt 1000MB } | stop-process
    ```

1. 计算一个目录下文件内的字节大小：

    ```powershell
    get-childitem | measure-object -property length -sum
    ```

1. 字符串大小写转换：

    ```powershell
    "hello, world!".ToUpper()
    ```

1. 订阅一个指定的RSS Feed并显示它最近8个主题：

    ```powershell
    $rssUrl = "http://blogs.msdn.com/powershell/rss.aspx"
    $blog = [xml](new-object System.Net.WebClient).DownloadString($rssUrl)
    $blog.rss.channel.item | select title -first 8
    ```

## 概念

### cmdlet

PS 的内置指令，念作 command-let。

### 别名 Alias

## 安装

```sh
sudo snap install --classic powershell
```

## 参考资料与拓展阅读

- <https://www.656463.com/wenda/Powershellchazhaobingyidongwupin_596>
- <https://docs.microsoft.com/zh-cn/powershell/scripting/overview?view=powershell-7>
