# cmdlet

## 通用参数

- `Debug` (db)
- `ErrorAction` (ea)
- `ErrorVariable` (ev)
- `InformationAction` (infa)
- `InformationVariable` (iv)
- `OutVariable` (ov)
- `OutBuffer` (ob)
- `PipelineVariable` (pv)
- `Verbose` (vb)
- `WarningAction` (wa)
- `WarningVariable` (wv)

## 常用命令

```powershell
$PSVersionTable

Name                           Value
----                           -----
PSVersion                      7.0.0
PSEdition                      Core
GitCommitId                    7.0.0
OS                             Linux 5.3.0-46-generic #38-Ubuntu SMP Fri Mar 27 17:37:05 UTC 2020
Platform                       Unix
PSCompatibleVersions           {1.0, 2.0, 3.0, 4.0…}
PSRemotingProtocolVersion      2.3
SerializationVersion           1.1.0.1
WSManStackVersion              3.0
```

### 文档系列

对于我这样的新人应该最先掌握的就是这三个命令了，学习什么新东西都是这样，先要学会熟练查询资料。

#### Get-Help

相当于 man

示例：`get-help write-host -examples`
更新文档：`Update-Help -Force -Verbose -ErrorAction SilentlyContinue`

```sh
Update-Help -Force -Ea SilentlyContinue -Ev ErrMsgDetail
```

#### Get-Command (gcm)

#### Get-History (history, h, ghy)

相当于 history

### mkdir, md

### Get-ChildItem (gci, dir)

相当于 ls

### Process 系列

- Start-Process 好比之前的 start 命令
- Stop-Process (kill, spps)
- Get-Process (ps, gps) 相当于 ps

### Alias 系列

alias 命令也可用

- Get-Alias (gal) 相当于 alias
- Set-Alias (sal)
- New-Alias (nal)
- Remove-Alias
- Export-Alias (epal)
- Import-Alias (ipal)

### Location 系列

- Get-Location (pwd, gl) 相当于 pwd
- Set-Location (cd, sl) 相当于 cd
- Pop-Location (popd)
- Push-Location (pushd)

### Content 系列

- Get-Content (type, gc)
- Set-Content
- Add-Content
- Clear-Content (clc)

相当于 cat

### Copy-Item (copy, cpi)

相当于 cp

### Move-Item (move, mi)

相当于 mv

### Remove-Item (del)

相当于 rm

### Write-Output (echo)

相当于 echo

- `write-verbose`
- `write-debug`
- `write-information`
- `write-warning`
- `write-error`
- `write-progress`

### Host 系列

- `clear-host`
- `$x = read-host 'Confirm? [Y]es OR [N]o'`
- `write-host`

### 网络相关

- `test-connection` 相当于 ping
- `invoke-webrequest`

## 参考资料与拓展阅读

- <https://docs.microsoft.com/zh-CN/powershell/module/microsoft.powershell.core/about/about_commonparameters?view=powershell-7>
