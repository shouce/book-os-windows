# 基础命令

```powershell
alias
% / foreach / foreach-object
? / write-object
cat / type/ get-content
cd / set-location
chdir / set-location
cli / clear-item
compare / diff / compare-object
cp / copy / copy-item
curl / wget / invoke-webrequest
del / rd / ri / ren / rmdir / remove-item
dir / ls / get-childitem
echo / write / write-output
fhx / format-hex
ghy / history / get-history
gm / get-member # 获取对象属性
gi / get-item
h / help / get-help
ii / invoke-item
ise / powershell_ise.exe
kill / stop-process
ls / get-children
man / help
md / mkdir
mv / move / move-item
nal / new-alias
ni / new-item
popd / pop-location
pushd / push-location
ps / get-process
pwd / get-location
r / invoke-history
rename / rni / rename-item
sleep / start-sleep
sls / select-string
sort / sort-object
start / start-process
tee / tee-object
where / where-object
```

## process

```powershell
ps -name *python*
ps | gm
ps | sls python | out-gridview
ps | sort -property vm -desc
ps | sort -property cpu -desc
```

## help

```powershell
help *-object
help ps -examples
```

## filter

```powershell

```
## export

```powershell
export-csv(epcsv) import-csv(ipcsv)
convertfrom-csv convertto-csv

export-clixml import-clixml
convertto-xml

convertto-html
convertto-json
```

比如：

```powershell
ps | convertto-json
ps | out-gridview # 控制台窗口
```
