# 命令行

- DOS，`command.com`，cmd (bat)：传统 Shell，类比 bash
- PowerShell：一个新式的，面向对象的 Shell，能解决问题就行

此外还有这三种脚本：

- cscript (wsh)
- vbscript
- jscript

## 工具

### cmder 图形终端

### chocolatey 包管理器

<https://github.com/cmderdev/cmder/releases/latest>

## 参考资料与拓展阅读

- <https://zh.wikipedia.org/wiki/Windows_Script_Host>
