# WSL（Windows Subsystem for Linux）

## 曾经

#### MinGW

Minimalist GNU for Windows

#### Cygwin

#### MSYS

## 参考

- 开源中国社区，技术翻译，[WSL 文件系统支持](http://www.oschina.net/translate/wsl-file-system-support)
- MSDN，[Windows Subsystem for Linux](https://blogs.msdn.microsoft.com/wsl/ "The underlying technology enabling the Windows Subsystem for Linux")
- GitHub，[serialphotog/winbuntu](https://github.com/serialphotog/winbuntu)
