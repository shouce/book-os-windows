# Windows 上的链接文件

## 复习：什么是链接

文件是磁盘上的一块区域，而文件名就是指向文件的一个指针。删除文件，就是删除指向文件的指针。

如果没有指针指向一个文件，那么我们就不能通过正常渠道读到这个文件了，而这个文件所在区域就会标记成可用。在下一次写这个区域之前，里面的内容就都还没有改变，我们可以通过一些方法找回，这也是数据恢复的原理。

- 硬链接：指向磁盘上的一个位置（node，节点）。  
    如果给文件 a 创建了一个硬链接 b，那么，a 和 b 是对等的，如果删除 a，还可以通过 b 这个“指针”访问那个文件。
- 软链接：指向了文件系统上的一个路径（path）。  
    如果给文件 a 创建了一个软链接 c，那么，c 指向的不是文件，而是 a 这个路径，如果删除 a，那么再访问 b 就会提示 a 不存在了，即便是还可以通过硬链接 b 访问到那个文件。

#### 文件夹

文件夹只是文件系统中的一种文件组织形式，逻辑上的东西，所以磁盘上并没有划分出一块区域，说这是这个文件夹的空间。

因此，硬链接只能指向文件。

#### 快捷方式

Windows 中的快捷方式只有 explorer（Windows 资源管理器） 认识，其实就是一个 `.lnk` 文件，点击这个文件就是调用 explorer 打开指定的路径，相当于一个传送门，和链接的原理完全不一样。

## `mklink` 命令

语法：

```
MKLINK [[/D] | [/H] | [/J]] Link Target

        /D      创建目录符号链接。默认为文件
                符号链接。
        /H      创建硬链接而非符号链接。
        /J      创建目录联接。
        Link    指定新的符号链接名称。
        Target  指定新链接引用的路径
                (相对或绝对)。
```

和 Linux 中一样的目录软链接，这里叫做目录联接。

```
mklink /J C:\Users\catroll\Projects D:\Projects
```

## `fsutil` 命令

```
fsutil hardlink create NewFileName ExistingFileName
```

## `ln` 命令

通过 MinGW、或者 Cygwin 中的 `ln` 命令。

像 Linux 中一样。

```
ln -s <TARGET_PATH> <LINK_NAME>
ln -d <TARGET_FILE> <LINK_NAME>
```

不过这里面的软链接感觉就是复制一份，和 Linux 中不一样。

## 参考

- 博客园，大众.NET，[Windows下硬链接、软链接和快捷方式的区别](http://www.cnblogs.com/heqichang/archive/2012/04/26/2471774.html)
- 博客园，plusium，[windows系统下的文件夹链接功能mklink/linkd](http://www.cnblogs.com/plusium/archive/2010/03/17/1688511.html)
