# 环境搭建记录

## 系统

- Windows 10 + KMSpico（激活程序）
	- 邮件客户端

## 基本工具

- 360 安全卫士
- 360 驱动大师
- 360 压缩
- Chrome 稳定版
- 腾讯 QQ
- WPS Office
- 搜狗输入法
- 迅雷

## 开发

#### 版本控制

- Git for Windows
- SourceTree
- TortoiseSVN + 中文语言包

#### 编辑器/IDE

- Notepad++
	- 知乎，[优化Markdown在Notepad++中的使用体验](http://www.jianshu.com/p/cdb42773fffe)
- PyCharm

#### 文档

- GitBook

#### 虚拟化

- VirtualBox
	- UubuntuGnome + tmux、openssh server、git、git-svn、gitg
- Vagrant

#### 程序设计语言

- Python 2.7.10
- Ruby
- XAMPP（PHP + MySQL + Apache）

#### 远程连接与文件传输

- FileZilla Client
- Xshell
- Xftp
- Putty 中文汉化版

## 网络存储

- 腾讯微云
- 360 网盘
- 金山快盘

## 其他

- Shadowsocks
- 飞秋 2013
- GIMP
